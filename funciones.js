
function Ejercicio1()
{
    var date = new Date();
    hora= date.getHours();
    minutos=date.getMinutes();
    segundos=date.getSeconds();
    alert("La hora actual es   " + hora + " : " + minutos +" : " + segundos );

    alert( "La hora actual en segundos es   " + hora*3600 + " : " + minutos*60 +" : " + segundos + " segundos");

}
function Ejercicio2() {
    var base = parseInt(prompt("Introduzca la base de un triangulo"));
    var altura = parseInt(prompt("Introduzca la altura de un triangulo"));
    alert("El area de un triangulo con  una base " + base + " cm  " + "  y  una altura  " + altura + 
        " cm  "+"  es : " + (base * altura)/2 );
    
}

function Ejercicio3() {
    var num  = parseInt(prompt("Introduzca un numero impar"));
    var raiz = Math.sqrt(num);
    var resultado  = 0;
        if (raiz < 10) {
        resultado = raiz.toFixed(2);
        } else {
        if (raiz < 100) {
            resultado = raiz.toFixed(1);
        } else {
            resultado = raiz.toFixed(0);
        }
        }
    alert("La raíz cuadrada del número " + num + " es : " + resultado);
    
  }
  function Ejercicio4() {
    var cadena = String(prompt("Introduzca cualquier texto"));
    alert("La longitud de la cadena ingresada es de : " + cadena.length + " caracteres");
    
  }
 
  
var array1 = ["Lunes", " Martes", " Miércoles", " Jueves", " Viernes"];
var array2 = [" Sábado", " Domingo"];
var array3 = array1.concat(array2);

function Ejercicio5() {
  alert( "La union del Vector uno y dos da como resultado el siguiente vector " + array3 ); 
}

function Ejercicio6() {
    alert('"La version del navegador es la siguiente " \n '+ navigator.appVersion);
}
function Ejercicio7() {
    alert("La resolucion de la panatalla es de "+ screen.width + " X " + screen.height ); 
}
function Ejercicio8() {
    if (parseInt(navigator.appVersion)>4)
    alert("Desea imprimir la pagina  web actual")
    window.print();
    
}